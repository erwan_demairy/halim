#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;
long reverse(long n) {
    long result = 0;
    while (n != 0) {
        result = result * 10 + (n % 10);
        n /= 10;
    }
    return result;
}

bool isPal(long n) {
    return n == reverse(n);
}
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
    int nbTc;
    cin >> nbTc;
    for (int tc = 0; tc < nbTc; tc++ ) {
        long N;
        cin >> N;
        int nbOps = 0;
        do {
            N = N + reverse(N);
            nbOps++;
        } while( not isPal(N) ) ;
        cout << nbOps << " " << N << endl;
    }
}

#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
    int nbTc;
    cin >> nbTc;
    for (int tc = 0; tc < nbTc; tc++ ) {
        long row, col;
        cin >> row >> col;
        long n = max( row, col );
        // interval = [ (n-1)^2 + 1, n^2 ]
        long result = 0;
        if (n % 2 == 1) {
            swap(row, col);
        }
        if (col==n) {
            result = (n-1)*(n-1) + 1 + row - 1;
        } else if (row==n) {
            result = (n-1)*(n-1) + 1 + n - 1 + (n-col);
        }
        cout << result << endl;
    }
}

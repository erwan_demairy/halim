#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
    int nbTc;
    cin >> nbTc;
    for (int tc = 0; tc < nbTc; tc++ ) {
        long farmerNumber, size, animals, env;
        long total = 0;
        cin >> farmerNumber;
        for (int f = 0; f < farmerNumber; f++) {
            cin >> size >> animals >> env;
            total += size * env;
        }
        cout << total << endl;
    }
}

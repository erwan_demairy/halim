#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;

long nb_moves(long k) {
/*
    OOOO
    OOOO
    OOOO
    OOOO
*/

    if (k == 1) {
        return 0;
    } else if (k==2) {
        return 6;
    } else if (k==3) {
        return 28;
    }
    long res = 0;
    // coins : 3 cases en moins
    res += 4 * ( k * k - 3 );

    // ligne et colonnes 1 et k
    //   autre pos = 2 ou k-1 : 4 cases en moins
    res += 8 * ( k * k - 4 );
    //   autre pos >=3 et <= k-2 : 5 cases en moins
    if (k > 4) {
        res += 4 * ( k - 4 ) * ( k * k - 5);
    }

    // ligne et colonnes 2 et k-1
    //  autre pos == 2 : moins 5 cases
    res += 4 * (k*k - 5);
    //  autre pos >= 3 et <= k-2 : moins 7 cases
    if (k > 4) {
        res += 4 * (k - 4) * (k * k - 7);
    }

    // ligne et colonnes >= 3 et <= k-3 : 9 cases en moins
    if (k > 4) {
        res += (k-4)*(k-4)*(k*k-9);
    }


    // symétrie entre les deux cavaliers.
    return res / 2;
}
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
    int N;
    cin >> N;
    for (int i = 1; i <= N; i++) {
      cout << nb_moves(i) << endl;
    }
}

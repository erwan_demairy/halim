project(UVA-299-TRAIN-SWAPPING)
cmake_minimum_required(VERSION 3.19)
set(CMAKE_CXX_FLAGS "-Wall --std=c++17")
enable_testing()

add_executable(${PROJECT_NAME} uva-299-train-swapping)

FILE(GLOB inputs input*.txt)
foreach(input ${inputs})
  string(REPLACE "input" "output" output ${input})
  string(REPLACE "input" "output_run" output_run ${input})
  message("input = ${input}")
  message("output = ${output}")
  message("output_run = ${output_run}")
  add_test(
      NAME "test_${input}"
      COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/../test_driver ${${PROJECT_NAME}_BINARY_DIR}/${PROJECT_NAME} ${input} ${output_run} ${output}
      WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}   
  )
  set_tests_properties("test_${input}" PROPERTIES TIMEOUT 2) 
endforeach()

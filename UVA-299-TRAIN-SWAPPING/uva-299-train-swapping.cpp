#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
    int nbTc;
    cin >> nbTc;
    for (int tc = 0; tc < nbTc; tc++ ) {
        int N;
        cin >> N;
        vector<int> A;
        for (int i=0; i<N; i++) {
            int v;
            cin >> v;
            A.push_back(v);
        }
        int ops = 0;
        for (int i = N-1; i >= 0 ; i--) {
            for (int j=0; j<i; j++) {
                if (A[j] > A[j+1]) {
                    ops++;
                    swap(A[j], A[j+1]) ;
                }
            }
        }
        cout << "Optimal train swapping takes " << ops << " swaps." << endl;
    }
}

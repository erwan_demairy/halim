#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <fstream>
#include <limits.h>
using namespace std;
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
    long N;
    cin >> N;
    long sum = (N * (N + 1)) / 2;
//    cerr << sum << endl;
    if (sum % 2 == 1) {
        cout << "NO" << endl;
    } else {
        cout << "YES" << endl;
        set<long> first;
        long value = sum / 2;
        long current = N;
        while (value > current) {
            first.insert(current);
            value -= current;
            current --;
        }
        if (value > 0) {
            first.insert(value );
        }
        cout << first.size() << endl;
        for (int v: first) {
            cout << v << " ";
        }
        cout << endl;
        cout << N - first.size() << endl;
        for (int v=1; v<=N; v++) {
            if (first.find(v) == first.end()) {
                cout << v << " ";
            }
        }
        cout << endl;
    }
}

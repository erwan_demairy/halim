#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!

    string s;
    cin >> s;
    char last = ' ';
    int sizeSeq = 0;
    int maxSize = 0;
    for (int i=0; i<s.size(); i++) {
        if (s[i] == last) {
            sizeSeq++;
        } else {
            maxSize = max(sizeSeq, maxSize);
            sizeSeq = 1;
            last = s[i];
        }
    }
    maxSize = max(sizeSeq, maxSize);

    cout << maxSize << endl;
}

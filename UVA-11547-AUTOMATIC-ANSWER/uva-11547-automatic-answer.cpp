#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
    int nbTc;
    cin >> nbTc;
    for (int tc = 0; tc < nbTc; tc++ ) {
        long N;
        cin >> N;
        // “Multiply n by 567, then divide the result by 9, then add 7492, then multiply by 235, then divide
        //by 47, then subtract 498. What is the digit in the tens column?”
        long result = N;
        result *= 567;
        result /= 9;
        result += 7492;
        result *= 235;
        result /= 47;
        result -= 498;
//        cout << result << endl;
        result = ((result / 10) % 10);

        cout << abs(result) << endl;
    }
}

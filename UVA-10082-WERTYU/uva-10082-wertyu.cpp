#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
#include <sstream>
using namespace std;

string dict[] = {
        "`1234567890-=\b",
        "\tQWERTYUIOP[]\\",
        "ASDFGHJKL;'\n",
        "ZXCVBNM,./"
};

char translate(char c) {
    if (c == ' ') {
        return ' ';
    }
    int pos;
    for (int i=0; i < 4; i++) {
        string currentLine = dict[i];
        pos = currentLine.find(c);
        if (pos == string::npos) {
            continue;
        } else {
            return currentLine[ pos - 1];
        }
    }
    if (pos == string::npos) {
        cerr << "Error searching for " << c << endl;
        return c;
    }
}
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!


    string input;
    while (true) {
        getline(cin, input);
        if (input.size() == 0) break;
        ostringstream os;
        for (char c: input) {
            os << translate(c);
        }
        cout << os.str() << endl;
    }
}

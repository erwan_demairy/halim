#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!

    int N;
    cin >> N;
    if (N==1) {
        cout << "1" << endl;
    } else if (N <= 3) {
        cout << "NO SOLUTION" << endl;
    } else {
        int half = N / 2;
        for (int i=2; i <= half*2; i += 2) {
            cout << i << " ";
        }
        if ( N % 2 == 0) {
            for (int i=1; i <= 2*half - 1; i += 2) {
                cout << i << " ";
            }
        } else {
            for (int i=1; i<= 2*half + 1; i += 2) {
                cout << i << " ";
            }
        }
        cout << endl;
    }
}

#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
    do {
        long N;
        cin >> N;
        if (N == 0) break;
        while(N > 9) {
            long reduce = 0;
            while (N>0) {
                reduce += (N%10);
                N /= 10;
            }
            N = reduce;
        }
        cout << N << endl;
    } while (true);
}

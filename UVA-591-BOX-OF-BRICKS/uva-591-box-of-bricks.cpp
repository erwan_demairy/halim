#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
    int cptTC = 1;
    do {
        int N;
        cin >> N;
        if (N == 0) break;
        long total = 0;
        vector<long> values;
        for (int i=0; i<N; i++) {
            long v;
            cin >> v;
            values.push_back( v );
            total += v;
        }
        long moyenne = total / N;
        long result = 0;
        for (int i=0; i<N; i++) {
            result += abs(values[i] - moyenne);
        }

        cout << "Set #" << cptTC << endl;
        cout << "The minimum number of moves is " << result / 2 << "." << endl << endl;
        cptTC++;
    } while (true);
}

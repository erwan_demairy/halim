#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!

    int N;
    cin >> N;
    long total = 0;
    long prec;
    cin >> prec;
    for (int i=0; i<N - 1; i++) {
        long v;
        cin >> v;
        if (v < prec) {
            total += (prec-v);
        } else {
            prec = v;
        }
    }

    cout << total << endl;
}

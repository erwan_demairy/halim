#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
    int nbTc;
    cin >> nbTc;
    for (int tc = 0; tc < nbTc; tc++ ) {
        int a;
        int b;
        cin >> a >> b;
        int a_prime = (a % 2 == 1) ? a / 2 : (a+1) / 2;
        int b_prime = (b % 2 == 1) ? b / 2 : (b-1) / 2;
        long sum = 2 * ( ((b_prime * (b_prime + 1)) / 2 - (((a_prime - 1) * ((a_prime - 1 ) + 1)) / 2))) + (b_prime - a_prime + 1);
        cout << "Case " << tc + 1 << ": " << sum << endl;
    }
}

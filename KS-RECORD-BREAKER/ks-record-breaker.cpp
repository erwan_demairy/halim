#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <limits.h>
using namespace std;
 
int main(int argc, char** argv) {
    if (argc == 3) {
        freopen(argv[1], "r", stdin);
        freopen(argv[2], "w", stdout);
        freopen(argv[3], "w", stderr);
    }
//    freopen("output.txt", "w", stdout);
//    std::ifstream in("test.txt");
//    std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
//    std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!
    int nbTc;
    cin >> nbTc;
    for (int tc = 0; tc < nbTc; tc++ ) {
        int N;
        cin >> N;
        vector<int> A;
        vector<int> Amin(N);
        for (int i=0; i<N; i++) {
            int v;
            cin >> v;
            A.push_back( v );
            if (i>0) {
                Amin[i] = max( Amin[i-1], A[i-1]);
            } else {
                Amin[i] = -1;
            }
        }
        int nRecord = 0;
        for (int i=0; i<N; i++) {
            bool isRecord =
                    (( i == 0 ) || (A[i] > Amin[i])) &&
                            (( i == N-1) || (A[i] > A[i+1]));
            if (isRecord) {
                nRecord++;
            }
        }
     
        cout << "Case #" << tc + 1 << ": " << nRecord << endl;
    }
}
